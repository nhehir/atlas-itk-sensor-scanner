# ATLAS ITk Sensor Scanner Documentation

This git repo serves as a storage place for the documentation for the ATLAS ITk sensor scanner designed
and built at Queen Mary University of London in 2018 and the software for operating it. The
documentation is formed of three components:

* User Guide
* Software Documentation
* Build Specifications

# Building

Call `make desired_file.pdf` to compile `desired_file.pdf` or call `make` in order to compile all
documents. Call `make clean all` to compile from scratch.