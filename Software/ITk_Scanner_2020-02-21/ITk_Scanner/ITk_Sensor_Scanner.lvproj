﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="2D_Scan.vi" Type="VI" URL="../2D_Scan.vi"/>
		<Item Name="Acquire_Image.vi" Type="VI" URL="../Acquire_Image.vi"/>
		<Item Name="Assign_XYZ.vi" Type="VI" URL="../Assign_XYZ.vi"/>
		<Item Name="Axes_Control.ctl" Type="VI" URL="../Axes_Control.ctl"/>
		<Item Name="Barcode_Handler.vi" Type="VI" URL="../Barcode_Handler.vi"/>
		<Item Name="Config.vi" Type="VI" URL="../Config.vi"/>
		<Item Name="Correct_Scan.vi" Type="VI" URL="../Correct_Scan.vi"/>
		<Item Name="Create_Scan_Directory.vi" Type="VI" URL="../Create_Scan_Directory.vi"/>
		<Item Name="Data_Control.ctl" Type="VI" URL="../Data_Control.ctl"/>
		<Item Name="Direction_Select.vi" Type="VI" URL="../Direction_Select.vi"/>
		<Item Name="Error_Handler.vi" Type="VI" URL="../Error_Handler.vi"/>
		<Item Name="Error_Shutdown.vi" Type="VI" URL="../Error_Shutdown.vi"/>
		<Item Name="Filename_Generator.vi" Type="VI" URL="../Filename_Generator.vi"/>
		<Item Name="Get_Scan_Parameters.vi" Type="VI" URL="../Get_Scan_Parameters.vi"/>
		<Item Name="Get_System_State.vi" Type="VI" URL="../Get_System_State.vi"/>
		<Item Name="Go_To_Scan_Start.vi" Type="VI" URL="../Go_To_Scan_Start.vi"/>
		<Item Name="Grab_Image.vi" Type="VI" URL="../Grab_Image.vi"/>
		<Item Name="Image_Format.ctl" Type="VI" URL="../Image_Format.ctl"/>
		<Item Name="Init.vi" Type="VI" URL="../Init.vi"/>
		<Item Name="Init_Controls.ctl" Type="VI" URL="../Init_Controls.ctl"/>
		<Item Name="Init_Controls.vi" Type="VI" URL="../Init_Controls.vi"/>
		<Item Name="Init_Data_Control.ctl" Type="VI" URL="../Init_Data_Control.ctl"/>
		<Item Name="ITk_State_Machine.vi" Type="VI" URL="../ITk_State_Machine.vi"/>
		<Item Name="Joystick_Events.vi" Type="VI" URL="../Joystick_Events.vi"/>
		<Item Name="Joystick_References.ctl" Type="VI" URL="../Joystick_References.ctl"/>
		<Item Name="logo.jpg" Type="Document" URL="../logo.jpg"/>
		<Item Name="Make_Scan_Arrays.vi" Type="VI" URL="../Make_Scan_Arrays.vi"/>
		<Item Name="MOV_or_MVR.vi" Type="VI" URL="../MOV_or_MVR.vi"/>
		<Item Name="Move_Absolute.vi" Type="VI" URL="../Move_Absolute.vi"/>
		<Item Name="Move_Absolute_Single.vi" Type="VI" URL="../Move_Absolute_Single.vi"/>
		<Item Name="Move_Relative.vi" Type="VI" URL="../Move_Relative.vi"/>
		<Item Name="Range_Calculator.vi" Type="VI" URL="../Range_Calculator.vi"/>
		<Item Name="Scan_Paramters.ctl" Type="VI" URL="../Scan_Paramters.ctl"/>
		<Item Name="Select_Fiducial.vi" Type="VI" URL="../Select_Fiducial.vi"/>
		<Item Name="Select_Sensor_Type.vi" Type="VI" URL="../Select_Sensor_Type.vi"/>
		<Item Name="Sensor_Types.ctl" Type="VI" URL="../Sensor_Types.ctl"/>
		<Item Name="Seq_Serial_Handler.vi" Type="VI" URL="../Seq_Serial_Handler.vi"/>
		<Item Name="Shutdown.vi" Type="VI" URL="../Shutdown.vi"/>
		<Item Name="State_Control.ctl" Type="VI" URL="../State_Control.ctl"/>
		<Item Name="UI_Handler.vi" Type="VI" URL="../UI_Handler.vi"/>
		<Item Name="View_Finder_Refresh_Once.vi" Type="VI" URL="../View_Finder_Refresh_Once.vi"/>
		<Item Name="Wait_For_Target.vi" Type="VI" URL="../Wait_For_Target.vi"/>
		<Item Name="Write_Image.vi" Type="VI" URL="../Write_Image.vi"/>
		<Item Name="XYZ_to_X_Y_Z.vi" Type="VI" URL="../XYZ_to_X_Y_Z.vi"/>
		<Item Name="XYZ_to_XY_Z.vi" Type="VI" URL="../XYZ_to_XY_Z.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
			</Item>
			<Item Name="#5.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/#5.vi"/>
			<Item Name="#5_old.vi" Type="VI" URL="../Dependencies/PI/Old commands.llb/#5_old.vi"/>
			<Item Name="#7.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/#7.vi"/>
			<Item Name="#24.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/#24.vi"/>
			<Item Name="*IDN?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/*IDN?.vi"/>
			<Item Name="Analog FGlobal.vi" Type="VI" URL="../Dependencies/PI/Analog control.llb/Analog FGlobal.vi"/>
			<Item Name="Analog Functions.vi" Type="VI" URL="../Dependencies/PI/Analog control.llb/Analog Functions.vi"/>
			<Item Name="Analog Receive String.vi" Type="VI" URL="../Dependencies/PI/Analog control.llb/Analog Receive String.vi"/>
			<Item Name="Assign booleans from string to axes.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Assign booleans from string to axes.vi"/>
			<Item Name="Assign NaN for chosen axes.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Assign NaN for chosen axes.vi"/>
			<Item Name="Assign values from string to axes.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Assign values from string to axes.vi"/>
			<Item Name="Available Analog Commands.ctl" Type="VI" URL="../Dependencies/PI/Analog control.llb/Available Analog Commands.ctl"/>
			<Item Name="Available DLL interfaces.ctl" Type="VI" URL="../Dependencies/PI/Communication.llb/Available DLL interfaces.ctl"/>
			<Item Name="Available DLLs.ctl" Type="VI" URL="../Dependencies/PI/Communication.llb/Available DLLs.ctl"/>
			<Item Name="Available interfaces.ctl" Type="VI" URL="../Dependencies/PI/Communication.llb/Available interfaces.ctl"/>
			<Item Name="Build command substring.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Build command substring.vi"/>
			<Item Name="Build query command substring.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Build query command substring.vi"/>
			<Item Name="C884_Configuration_Setup.vi" Type="VI" URL="../Dependencies/PI/C884_Configuration_Setup.vi"/>
			<Item Name="Close connection if open.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Close connection if open.vi"/>
			<Item Name="Commanded axes connected?.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Commanded axes connected?.vi"/>
			<Item Name="Commanded stage name available?.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Commanded stage name available?.vi"/>
			<Item Name="Controller names.ctl" Type="VI" URL="../Dependencies/PI/General command.llb/Controller names.ctl"/>
			<Item Name="CST handler.vi" Type="VI" URL="../Dependencies/PI/Support.llb/CST handler.vi"/>
			<Item Name="CST.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/CST.vi"/>
			<Item Name="CST?.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/CST?.vi"/>
			<Item Name="Cut out additional spaces.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Cut out additional spaces.vi"/>
			<Item Name="Define axes to command from boolean array.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Define axes to command from boolean array.vi"/>
			<Item Name="Define connected axes.vi" Type="VI" URL="../Dependencies/PI/General command.llb/Define connected axes.vi"/>
			<Item Name="Define connected stages with dialog.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Define connected stages with dialog.vi"/>
			<Item Name="Define connected systems (Array).vi" Type="VI" URL="../Dependencies/PI/General command.llb/Define connected systems (Array).vi"/>
			<Item Name="ERR?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/ERR?.vi"/>
			<Item Name="Find host address.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Find host address.vi"/>
			<Item Name="FNL.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/FNL.vi"/>
			<Item Name="FPL.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/FPL.vi"/>
			<Item Name="FRF.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/FRF.vi"/>
			<Item Name="FRF?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/FRF?.vi"/>
			<Item Name="GCSTranslateError.vi" Type="VI" URL="../Dependencies/PI/Support.llb/GCSTranslateError.vi"/>
			<Item Name="GCSTranslator DLL Functions.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/GCSTranslator DLL Functions.vi"/>
			<Item Name="GCSTranslator.dll" Type="Document" URL="../Dependencies/PI/GCSTranslator.dll"/>
			<Item Name="General wait for movement to stop.vi" Type="VI" URL="../Dependencies/PI/Support.llb/General wait for movement to stop.vi"/>
			<Item Name="Get all axes.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Get all axes.vi"/>
			<Item Name="Get arrays without blanks.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Get arrays without blanks.vi"/>
			<Item Name="Get lines from string.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Get lines from string.vi"/>
			<Item Name="Get subnet.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Get subnet.vi"/>
			<Item Name="Global Analog.vi" Type="VI" URL="../Dependencies/PI/Analog control.llb/Global Analog.vi"/>
			<Item Name="Global DaisyChain.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Global DaisyChain.vi"/>
			<Item Name="Global1.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Global1.vi"/>
			<Item Name="Global2 (Array).vi" Type="VI" URL="../Dependencies/PI/General command.llb/Global2 (Array).vi"/>
			<Item Name="HLP?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/HLP?.vi"/>
			<Item Name="Initialize Global1.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Initialize Global1.vi"/>
			<Item Name="Initialize Global2.vi" Type="VI" URL="../Dependencies/PI/General command.llb/Initialize Global2.vi"/>
			<Item Name="Is command present in HLP answer?.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Is command present in HLP answer?.vi"/>
			<Item Name="Is DaisyChain open.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Is DaisyChain open.vi"/>
			<Item Name="LIM?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/LIM?.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="MOV.vi" Type="VI" URL="../Dependencies/PI/General command.llb/MOV.vi"/>
			<Item Name="MOV?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/MOV?.vi"/>
			<Item Name="Move axes to their middle position.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Move axes to their middle position.vi"/>
			<Item Name="MvCameraControl.dll" Type="Document" URL="MvCameraControl.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="MvCameraControl.lvlib" Type="Library" URL="../Dependencies/HIKVISION/MvLVLib/MvCameraControl.lvlib"/>
			<Item Name="MvCameraPatch.dll" Type="Document" URL="MvCameraPatch.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="MvCameraPatch.dll" Type="Document" URL="../Dependencies/MvLVLib/x86/MvCameraPatch.dll"/>
			<Item Name="MVR.vi" Type="VI" URL="../Dependencies/PI/General command.llb/MVR.vi"/>
			<Item Name="ONT?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/ONT?.vi"/>
			<Item Name="PI Open Interface of one system.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/PI Open Interface of one system.vi"/>
			<Item Name="PI Receive String.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/PI Receive String.vi"/>
			<Item Name="PI Send String.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/PI Send String.vi"/>
			<Item Name="PI VISA Receive Characters.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/PI VISA Receive Characters.vi"/>
			<Item Name="POS?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/POS?.vi"/>
			<Item Name="Return space.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Return space.vi"/>
			<Item Name="RON.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/RON.vi"/>
			<Item Name="RON?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/RON?.vi"/>
			<Item Name="SAI?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/SAI?.vi"/>
			<Item Name="Select host address.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Select host address.vi"/>
			<Item Name="Select USB device.vi" Type="VI" URL="../Dependencies/PI/Communication.llb/Select USB device.vi"/>
			<Item Name="Set RON and return RON status.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Set RON and return RON status.vi"/>
			<Item Name="STA?.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/STA?.vi"/>
			<Item Name="String with ASCII code conversion.vi" Type="VI" URL="../Dependencies/PI/Support.llb/String with ASCII code conversion.vi"/>
			<Item Name="Substract axes array subset from axes array.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Substract axes array subset from axes array.vi"/>
			<Item Name="SVO.vi" Type="VI" URL="../Dependencies/PI/General command.llb/SVO.vi"/>
			<Item Name="SVO?.vi" Type="VI" URL="../Dependencies/PI/General command.llb/SVO?.vi"/>
			<Item Name="Termination character.ctl" Type="VI" URL="../Dependencies/PI/Communication.llb/Termination character.ctl"/>
			<Item Name="TMN?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/TMN?.vi"/>
			<Item Name="TMX?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/TMX?.vi"/>
			<Item Name="TRS?.vi" Type="VI" URL="../Dependencies/PI/Limits.llb/TRS?.vi"/>
			<Item Name="VST?.vi" Type="VI" URL="../Dependencies/PI/Special command.llb/VST?.vi"/>
			<Item Name="Wait for axes to stop.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Wait for axes to stop.vi"/>
			<Item Name="Wait for controller ready.vi" Type="VI" URL="../Dependencies/PI/Support.llb/Wait for controller ready.vi"/>
			<Item Name="Wait for hexapod system axes to stop.vi" Type="VI" URL="../Dependencies/PI/Old commands.llb/Wait for hexapod system axes to stop.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
