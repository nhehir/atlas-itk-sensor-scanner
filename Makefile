# You want latexmk to *always* run, because make does not have all the info.
# Also, include non-file targets in .PHONY so they are run regardless of any
# file of the given name existing.
.PHONY: atlas-itk-sensor-scanner.pdf build-specifications.pdf user-guide.pdf \
        software-documentation.pdf all clean directories

# The first rule in a Makefile is the one executed by default ("make"). It
# should always be the "all" rule, so that "make" and "make all" are identical.
all: atlas-itk-sensor-scanner.pdf build-specifications.pdf user-guide.pdf software-documentation.pdf \
directories

# CUSTOM BUILD RULES

# In case you didn't know, '$@' is a variable holding the name of the target,
# and '$<' is a variable holding the (first) dependency of a rule.
# "raw2tex" and "dat2tex" are just placeholders for whatever custom steps
# you might have.

#%.tex: %.raw
#        ./raw2tex $< > $@
#
#%.tex: %.dat
#        ./dat2tex $< > $@


# MAIN LATEXMK RULE
DIR       = $(shell pwd)
LATEXMK   = latexmk
OPTIONS   = -pdf -pdflatex="lualatex" -interaction=nonstopmode -use-make -f
MASTER    = -jobname=BUILD/atlas-itk-sensor-scanner atlas-itk-sensor-scanner.tex
BUILDSPEC = -jobname=BUILD/build-specifications BuildSpecifications/build-specifications.tex
USERGUIDE = -jobname=BUILD/user-guide UserGuide/user-guide.tex
SOFTWARE  = -jobname=BUILD/software-documentation SoftwareDocumentation/software-documentation.tex
CLEAN     = -C atlas-itk-sensor-scanner.pdf build-specifications.pdf user-guide.pdf \
software-documentation.pdf
MKDIR_P = mkdir -p
OUT_DIR = BUILD

# -pdf tells latexmk to generate PDF directly (instead of DVI).
# -pdflatex="" tells latexmk to call a specific backend with specific options.
# -use-make tells latexmk to call make for generating missing files.

# -interaction=nonstopmode keeps the pdflatex backend from stopping at a
# missing file reference and interactively asking you for an alternative.

directories: ${OUT_DIR}

${OUT_DIR}:
	${MKDIR_P} ${OUT_DIR}

atlas-itk-sensor-scanner.pdf: atlas-itk-sensor-scanner.tex directories
	$(LATEXMK) $(OPTIONS) $(MASTER) && mv BUILD/*.pdf .

build-specifications.pdf: BuildSpecifications/build-specifications.tex build
	$(LATEXMK) $(OPTIONS) $(BUILDSPEC) && mv BUILD/*.pdf .

user-guide.pdf: UserGuide/user-guide.tex build
	$(LATEXMK) $(OPTIONS) $(USERGUIDE) && mv BUILD/*.pdf .

software-documentation.pdf: SoftwareDocumentation/software-documentation.tex build
	$(LATEXMK) $(OPTIONS) $(SOFTWARE) && mv BUILD/*.pdf .

clean:
	if [ -d "BUILD" ]; then \
	rm -r BUILD; \
	fi
