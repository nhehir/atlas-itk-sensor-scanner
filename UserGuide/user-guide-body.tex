\section{User Guide: Introduction}%
\label{sec:ug-intro}
%
The ATLAS ITk sensor scanner is designed to perform a scan of over an area capturing an image at each
step. Whilst the current design is intended to scan over a 100~mm~x~100~mm area and image at a
resolution where a pixel in the captured image corresponds to approximately 1~$\mu$m~x~1~$\mu$m it is
possible to exchange components in order to suit different needs. What follows are instructions
detailing how to setup the hardware (section \ref{sec:hardware-setup}) which includes subsections
on the motor controller and motors, camera and optics and LED controller and lighting. Next are
instructions on how to setup the software in section \ref{sec:software-setup}, information about
inputting the initial settings for operation in section \ref{sec:init-settings}, details of the idle
state of the machine, including all available commands to the user in section \ref{sec:idle-state} and
finally information on how to start a scan of a sensor in section \ref{sec:start-scan}.

\section{Hardware setup}%
\label{sec:hardware-setup}

The following is guide for setting up the hardware, for full assembly read the build specifications.

\subsection{Motor controller and motors}

Ensure that the stages are connected to the motor controller via the serial lines. Make note of which
axis is connected to which port of the controller. Connect the controller to the PC via the method
of your choice. It is recommended that unless a fixed ip address has been assigned to the controller
that this is done via USB. Connect the camera to the PC via ethernet, if the PC does not support
power over ethernet (POE). Earth the motor controller via the earthing pin near the power delivery
port. Only once all connections are made should power be delivered to the controller or any of the
stages. Connect the XVDC power supplies to the stages and the YVDC power supply to the controller. 

\subsection{Camera and optics}

Make sure that the bracket with the M2 (check this) mounting point is attached to the bottom of the
camera. Remove the cover over the sensor and screw on the lens. Attach the camera to one of the three
holes on the camera mounting plate in accordance with the working distance of your lens. If the lens
cap is on then remove it before capturing any images or looking at the viewfinder. Be sure not to put
force on the platform of the z stage by tugging at the camera or its mounting platform. The best way
to remove the lens cap safely is to pry it off with one hand whilst holding with your other hand and
applying an opposing force such that the camera is not dragged in the direction of the lens cap.

\subsection{LED controller and lighting}

The LED controller is used to power the lighting as well as control brightness. It can also supply
power to the camera if power over ethernet is not available on the PC.%

\section{Software setup}%
\label{sec:software-setup}

\input{UserGuide/software-fig.tex}
\noindent
Setting up the software itself is very simple, all that is required is a LabVIEW installation
(developed and tested on 2017 version). There are also several dependencies that must be on the system
these are distributed with the software in a folder called dependencies. These include the windows
drivers for the PI motor controller and the MVS SDK which allows interaction with HIKVISION cameras. To
start the software find the file ``ITk\_State\_Machine.vi'' and open it in LabVIEW. You will be
prompted to show LabVIEW the location of a number of windows dynamic link library files (.dll) which
are included in the aforementioned dependencies folder. An image of the front panel that should be
visible upon opening the software is shown in figure~\ref{fig:front-panel}.

\section{Initial Settings}%
\label{sec:init-settings}

\input{UserGuide/initial-settings-fig.tex}
\noindent
The initial settings panel is labeled as such and lives in the top left corner of the front panel, it
is shown in figure~\ref{fig:initial-settings}. In this panel a number of settings must be set before
the scanner can be operated. Firstly the camera that is connected to the PC that you wish to use for
scanning must be selected from the drop-down list of available devices. If the camera you wish to use
does not appear or was not connected when the software was started the list can be refreshed by
pressing the blue ``Refresh Devices'' button. Next the user must selected which port the X, Y and Z
axes are connected to. The ports on the PI motor controllers are labeled with a number from 1 --- 4 in
the case of the C-884.4DC and the X and Y axes are labeled on the base of the aluminum framework. The
Z axes has the camera mounted on it and is oriented orthogonally to the X and Y axes. Finally the user
must select an output directory for the images recorded by the camera. Both images recorded during a
scan and those captured manually will be saved inside sub-directories of the selected output directory.
Images from a scan are saved inside a new directory named with the full serial number of the sensor
being scanned and images captured manually will be saved in a directory called manual\_captures. When
the initial settings have been entered correctly the user should press the green ``Continue'' button.

\section{The ``idle'' state}%
\label{sec:idle-state}

Once the initial settings have been entered and the continue button has been pressed the scanner will
transition into the ``idle'' state. In this state the scanner will sit and wait for commands from the
user. Available commands are summarised the table~\ref{tbl:idle-coms}.
%
\input{UserGuide/command-table.tex}
%
As well as the above commands there are a number of parameters that can be set in the idle state that
will affect the behaviour of the commands. These are listed in table~\ref{tbl:idle-params}.
%
\input{UserGuide/parameter-table.tex}
Giving the scanner a command usually means that it will transition into a state other than idle, this
can be seen in the state and error display. The scanner may transition through a number of states
before eventually returning to idle. Users must wait until the scanner is in idle again before their
commands will be registered.
%
\section{Starting a scan}%
\label{sec:start-scan}
In order to start a scan the user must first set the location of four fiducial markers in the software.
This is achieved by using the directional buttons to move the red cross-hair in the viewfinder over the
centre of the fiducial marker and pressing the select fiducial button. At each location the user should
manually optimise the focus of the camera by tweaking the Z height using the directional buttons.
Once all four fiducial locations have been input the user may start a scan by pressing the start scan
button. The area scanned is calculated by using the range of the supplied X and Y coordinates meaning
that additional area surrounding the sensor may be scanned in the situation where the sensor is not
rectangular or aligned correctly. The scan takes place at a Z value equal to the mean of the four Z
values from the fiducial locations. If the sensor is not flat within a certain tolerance then some
images may be out of focus when captured.
